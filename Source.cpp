#include <iostream>
#include <string.h>
using namespace std;

class Animal
{
protected:

	string Voiceline;

public:

	void virtual Voice()
	{
		cout << Voiceline;
	}
};

class Dog :  public Animal
{
private:

	string VoiceDog = "Woof";

public:

	void Voice() override
	{
		cout << "Dog says " << "Woof" << '\n';
	}

};

class Cat : public Animal
{
private:

	string VoiceCat = "Meow";

public:

	void Voice() override
	{
		cout << "Cats says " << VoiceCat << '\n';
	}
};

class Cow : public Animal
{
private:

	string VoiceCow = "Moo";

public:

	void Voice() override
	{
		cout <<"Cow says " << VoiceCow << '\n';
	}
};

int main()
{
	Animal* Bark = new Dog;
	Bark->Voice();

	Animal* Meow = new Cat;
	Meow->Voice();

	Animal* Moo = new Cow;
	Moo->Voice();

	int* test = new int[3]{ 1,2,3 };
	for (int i = 0; i < 3; i++)
	{
		cout << test[i] << '\n';
	}
	

	cout << "Creating Dynamic Array of pointers Voices which type is pointers to pointers? Animal**" << '\n'
		<< "with data type pointers to Class Animal subclasses new Dog, new Cat, new Cow, then using loop for" << '\n'
		<< "creating subclasses dependent on pointer in Voices [i] and calling overloaded function Voice() in them" << '\n';

	Animal** Voices = new Animal*[3]{new Dog,new Cat,new Cow};
	for (int i = 0; i < 3; i++)
	{
		Animal* Sound = Voices[i];
		Sound->Voice();
	}


}